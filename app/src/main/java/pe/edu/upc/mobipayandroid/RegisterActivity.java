package pe.edu.upc.mobipayandroid;

import android.app.DatePickerDialog;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.View;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Toast;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.JsonHttpResponseHandler;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.entity.StringEntity;
import cz.msebera.android.httpclient.message.BasicHeader;

/**
 * Created by Jurguen on 13/09/2016.
 */
public class RegisterActivity extends AppCompatActivity {

    //private Button btnDatePicker;
    //private Button btnFechaNacimiento;
    private EditText txtNombre;
    private EditText txtApellido;
    private EditText txtDni;
    //private EditText txtDate;
    private EditText txtDireccion;
    //private EditText txtDateNacimiento;
    private EditText txtCelular;
    private EditText txtEmail;
    private EditText txtPassword;
    private int mYear, mMonth, mDay;
    //private AutoCompleteTextView mEmailView;
    //private EditText mPasswordView;
    private Button btnSignup;
    private CoordinatorLayout coordinatorLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        coordinatorLayout = (CoordinatorLayout) findViewById(R.id.coordinatorLayoutRegister);

        txtNombre = (EditText) findViewById(R.id.in_nombres);
        txtApellido = (EditText) findViewById(R.id.in_apellidos);
        txtDni = (EditText) findViewById(R.id.in_dni);
        /*
        btnDatePicker=(Button)findViewById(R.id.btn_date);
        txtDate=(EditText)findViewById(R.id.in_date);
        btnDatePicker.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                activarCalendario(view, "E");
            }
        });
        */
        txtDireccion = (EditText) findViewById(R.id.in_direccion);
        /*
        txtDateNacimiento = (EditText) findViewById(R.id.in_date_nacimiento);
        btnFechaNacimiento = (Button) findViewById(R.id.btn_date_nacimiento);
        btnFechaNacimiento.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                activarCalendario(view, "N");
            }
        });
        */
        txtCelular = (EditText) findViewById(R.id.in_celular);
        txtEmail = (EditText) findViewById(R.id.in_email);
        txtPassword = (EditText) findViewById(R.id.in_password);

        btnSignup = (Button) findViewById(R.id.btn_signup);
        btnSignup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                registrarUsuario();
            }
        });
    }

    /*
    public void activarCalendario(View v, final String tipo) {
        // Get Current Date
        final Calendar c = Calendar.getInstance();

        mYear = c.get(Calendar.YEAR);
        mMonth = c.get(Calendar.MONTH);
        mDay = c.get(Calendar.DAY_OF_MONTH);

        DatePickerDialog datePickerDialog = new DatePickerDialog(this,
                new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year,
                                          int monthOfYear, int dayOfMonth) {
                        if (tipo.equals("E")) {
                            txtDate.setText(dayOfMonth + "/" + (monthOfYear + 1) + "/" + year);
                        }else{
                            txtDateNacimiento.setText(dayOfMonth + "/" + (monthOfYear + 1) + "/" + year);
                        }
                    }
                }, mYear, mMonth, mDay);
        datePickerDialog.show();
    }
    */

    private void registrarUsuario() {
        txtNombre.setError(null);
        txtEmail.setError(null);

        final String email = txtEmail.getText().toString();
        final String password = txtPassword.getText().toString();
        final String dni = txtDni.getText().toString();

        if (txtNombre.getText().toString().isEmpty()){
            txtNombre.setError(getString(R.string.error_field_required));
            txtNombre.requestFocus();
            return;
        }

        if (txtApellido.getText().toString().isEmpty()){
            txtApellido.setError(getString(R.string.error_field_required));
            txtApellido.requestFocus();
            return;
        }

        if (txtDni.getText().toString().isEmpty()){
            txtDni.setError(getString(R.string.error_field_required));
            txtDni.requestFocus();
            return;
        }

        if (txtDni.getText().length() != 8){
            txtDni.setError("DNI no válido");
            txtDni.requestFocus();
            return;
        }

        /*
        if (txtDate.getText().toString().isEmpty()){
            txtDate.setError(getString(R.string.error_field_required));
            txtDate.requestFocus();
            return;
        }
        */

        if (txtDireccion.getText().toString().isEmpty()){
            txtDireccion.setError(getString(R.string.error_field_required));
            txtDireccion.requestFocus();
            return;
        }
        /*
        if (txtDateNacimiento.getText().toString().isEmpty()){
            txtDateNacimiento.setError(getString(R.string.error_field_required));
            txtDateNacimiento.requestFocus();
            return;
        }
        */

        if (txtCelular.getText().toString().isEmpty()){
            txtCelular.setError(getString(R.string.error_field_required));
            txtCelular.requestFocus();
            return;
        }

        if (txtCelular.getText().length() != 9){
            txtCelular.setError("Celular no válido");
            txtCelular.requestFocus();
            return;
        }

        // Check for a valid email address.
        if (TextUtils.isEmpty(email)) {
            txtEmail.setError(getString(R.string.error_field_required));
            txtEmail.requestFocus();
            return;
        } else if (!isEmailValid(email)) {
            txtEmail.setError(getString(R.string.error_invalid_email));
            txtEmail.requestFocus();
            return;
        }

        // Check for a valid password, if the user entered one.
        //if (TextUtils.isEmpty(password) || !isPasswordValid(password)) {
        if (TextUtils.isEmpty(password)) {
            txtPassword.setError(getString(R.string.error_field_required));
            txtPassword.requestFocus();
            return;
        }

        // Show a progress spinner, and kick off a background task to
        // perform the user login attempt.

        /*
        showProgress(true);
        mAuthTask = new UserLoginTask(email, password);
        mAuthTask.execute((Void) null);
        */

        //mLoginButton.setEnabled(false);

        final ProgressDialog progressDialog = new ProgressDialog(RegisterActivity.this,R.style.AppTheme_Dark_Dialog);
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("Registrando usuario...");
        progressDialog.show();

        //String email = mEmailView.getText().toString();
        //String password = mPasswordView.getText().toString();

        AsyncHttpClient client = new AsyncHttpClient();
        JSONObject jsonParams = new JSONObject();
        try {
            jsonParams.put("Apellidos", txtApellido.getText().toString());
            jsonParams.put("Celular", txtCelular.getText().toString());
            jsonParams.put("Direccion", txtDireccion.getText().toString());
            jsonParams.put("Dni", txtDni.getText().toString());
            jsonParams.put("Mail", txtEmail.getText().toString());
            jsonParams.put("Nombres", txtNombre.getText().toString());
            jsonParams.put("Clave", password);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        StringEntity entity = null;
        try {
            entity = new StringEntity(jsonParams.toString());
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        UsuarioRestClient.post(RegisterActivity.this, "usuarios", entity, "application/json", new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                todoOk();
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                try {
                    String mensaje = new String(responseBody, "UTF-8");
                    txtDni.requestFocus();
                    Snackbar.make(coordinatorLayout, mensaje, Snackbar.LENGTH_LONG).show();
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
                progressDialog.dismiss();
            }
        });

        /*
            new android.os.Handler().postDelayed(
                    new Runnable() {
                        public void run() {
                            // TODO: Implement your own authentication logic here.
                            if(email.equals("test@upc.edu.pe") && dni.equals("33333333")){
                                maximoRegistro();
                            }else{
                                if(txtNombre.getText().toString().equals("Error")){
                                    errorConexionRegistro();
                                }else{
                                    todoOk();
                                }
                            }
                            progressDialog.dismiss();
                        }
                    }, 3000);*/
    }

    private void maximoRegistro(){
        Snackbar.make(coordinatorLayout, "Se superó el máximo de cuentas por DNI (3)", Snackbar.LENGTH_LONG).show();
    }

    private void errorConexionRegistro(){
        Snackbar.make(coordinatorLayout, "Error al realizar conexión", Snackbar.LENGTH_LONG).show();
    }

    private void todoOk(){
        /*
        coordinatorLayout = (CoordinatorLayout) findViewById(R.id.coordinatorLayout);
        Snackbar.make(coordinatorLayout, "Usuario Registrado", Snackbar.LENGTH_LONG).show();
        */
        setResult(RESULT_OK);
        finish();
    }

    private boolean isEmailValid(String email) {
        //TODO: Replace this with your own logic
        return email.contains("@");
    }

    private boolean isPasswordValid(String password) {
        //TODO: Replace this with your own logic
        return password.length() > 4;
    }

}
