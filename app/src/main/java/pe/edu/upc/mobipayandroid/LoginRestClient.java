package pe.edu.upc.mobipayandroid;

import android.content.Context;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import cz.msebera.android.httpclient.HttpEntity;

/**
 * Created by Jurguen on 24/09/2016.
 */

public class LoginRestClient {
    private static final String BASE_URL = "http://sistemas.apphb.com/AccesoUsuariosService.svc/";

    private static AsyncHttpClient client = new AsyncHttpClient();

    public static void get(String url, RequestParams params, AsyncHttpResponseHandler responseHandler) {
        client.get(getAbsoluteUrl(url), params, responseHandler);
    }

    public static void post(Context context, String url, HttpEntity entity, String contentype, AsyncHttpResponseHandler responseHandler) {
        client.post(context,getAbsoluteUrl(url),entity,contentype,responseHandler);
        //client.post(getAbsoluteUrl(url), params, responseHandler);
    }

    private static String getAbsoluteUrl(String relativeUrl) {
        return BASE_URL + relativeUrl;
    }
}
